import Router from './Router';
import data from './data';
import PizzaList from './pages/PizzaList';
import Component from './components/Component';

Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = document.querySelector('.pageContent');
Router.menuElement = document.querySelector('.mainMenu');

const pizzaList = new PizzaList([]);
const aboutPage = new Component('section', null, 'Ce site est génial');
const pizzaForm = new Component(
	'section',
	null,
	'Ici vous pourrez ajouter une pizza'
);

Router.routes = [
	{ path: '/', page: pizzaList, title: 'La carte' },
	{ path: '/a-propos', page: aboutPage, title: 'À propos' },
	{ path: '/ajouter-pizza', page: pizzaForm, title: 'Ajouter une pizza' },
];

Router.navigate('/'); // affiche une page vide
pizzaList.pizzas = data;
Router.navigate('/'); // affiche la liste des pizzas

const news = document.querySelector('.newsContainer');
news.setAttribute('style', '');

const button = document.querySelector('.closeButton');
button.addEventListener('click', HideContent);
function HideContent(event) {
	event.preventDefault();
	news.setAttribute('style', 'display: none;');
}
