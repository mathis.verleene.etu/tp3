export default class Router {
	static titleElement;
	static contentElement;
	static routes = [];
	static #menuElement;
	static set menuElement(element) {
		this.#menuElement = element;
		document.querySelectorAll('.mainMenu li a').forEach(e => {
			e.addEventListener('click', onClickMenu);
		});

		function onClickMenu(path) {
			Router.navigate(path);
		}
	}
	static navigate(path) {
		const route = this.routes.find(route => route.path === path);
		if (route) {
			this.titleElement.innerHTML = `<h1>${route.title}</h1>`;
			this.contentElement.innerHTML = route.page.render();
		}
	}
}
